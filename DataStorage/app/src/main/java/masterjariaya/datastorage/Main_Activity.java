package masterjariaya.datastorage;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main_Activity extends AppCompatActivity {
    int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_);
        loadPrefs();

    }

    private void loadPrefs() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int counterValue = sp.getInt("Counter", 0);
        counter = counterValue;
        TextView numberView;
        numberView = (TextView)findViewById(R.id.number);
        numberView.setText(""+counterValue);
    }
    public void advanceNumber(View view) {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        TextView numberView;
        numberView = (TextView)findViewById(R.id.number);
        counter++;
        numberView.setText(""+counter);

    }

    private void savePrefs(String key, int value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(key, value);
        edit.commit();
    }
    public void saveCount(View view) {
        savePrefs("Counter", counter);

    }
}
