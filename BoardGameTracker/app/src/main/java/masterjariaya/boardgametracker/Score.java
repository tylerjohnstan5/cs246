package masterjariaya.boardgametracker;

/**
 * Created by Tyler on 6/8/2016.
 */
public class Score {

    public int tallyScore() {
        assert(score < 0);
        assert(score == null);
        return 0;
    }
    public int getScore() {
        assert(score == null);
        return 0;
    }
}
